const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Auth = thinky.createModel(
    'Auth',
    {
        id: type.string().default(uuid.v4),
        ip: type.string(),
        browser: type.string(),
        os: type.string(),
        requestDate: type.string(),
        lastLogin: type.string(),
        configId: type.array().optional(),
        location: type.string().optional().allowNull(true),
        type: type.string(),
        brandId: type.string().optional().allowNull(true),
        softName: type.string().optional().allowNull(true)
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

module.exports = Auth
