const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

const GH_Race = thinky.createModel(
    'GH_Race',
    {
        _race_id: type.string(),
        match_url: type.string().optional(),
        race_card: type
            .object()
            .optional()
            .allowExtra(true)
            .allowNull(true),
        runners: type
            .array()
            .optional()
            .allowNull(true),
        race: type
            .object()
            .optional()
            .allowExtra(true)
            .allowNull(true),
        statistics: type
            .object()
            .optional()
            .allowExtra(true)
            .allowNull(true),
        race_details: type
            .object()
            .optional()
            .allowExtra(true)
            .allowNull(true),
        predictor: type
            .array()
            .optional()
            .allowNull(true),
        post_picks: type
            .array()
            .optional()
            .allowNull(true),
        hot_traps: type
            .array()
            .optional()
            .allowNull(true),
        career_form: type
            .array()
            .optional()
            .allowNull(true),
        early_leaders: type
            .array()
            .optional()
            .allowNull(true),
        top_tricast: type
            .array()
            .optional()
            .allowNull(true),
        top_form: type
            .array()
            .optional()
            .allowNull(true),
        bet_prompts: type
            .object()
            .optional()
            .allowExtra(true)
            .allowNull(true)
    },
    {
        conflict: 'replace',
        pk: '_race_id'
    }
)

module.exports = GH_Race
