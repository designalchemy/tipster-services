const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Dashboard = thinky.createModel(
    'Dashboard',
    {
        _dashboard_id: type.string().default(uuid.v4),
        dashboard_brand_id: type.string(),
        dashboard_act_id: type.string(),
        dashboard_race_ids: type.array(),
        config: type.object().optional().allowExtra(true).allowNull(true),
    },
    {
        conflict: 'replace',
        pk: '_dashboard_id',
    }
)

module.exports = Dashboard
