const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

const Odds = thinky.createModel(
    'Odds',
    {
        _odd_id: type.string().default(uuid.v4),
        bookmaker_id: type.string(),
        odds_data: type.object().optional().allowExtra(true).allowNull(true)
    },
    {
        conflict: 'update',
        pk: 'bookmaker_id'
    }
)

module.exports = Odds
