const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Config = thinky.createModel(
    'Config',
    {
        id: type.string().default(uuid.v4),
        soft_name: type.string(),
        config_brand_id: type.string(),
        config: type.object().optional().allowExtra(true).allowNull(true)
    },
    {
        conflict: 'replace',
        pk: 'id'
    }
)

module.exports = Config
