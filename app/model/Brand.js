const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Brand = thinky.createModel('Brand', {
    id: type.string().default(uuid.v4),
    brand_name: type.string(),
    brand_logo: type.string()
})

module.exports = Brand
