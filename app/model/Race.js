const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

const Race = thinky.createModel(
    'Race',
    {
        _race_id: type.string(),
        predictor: type.object().optional().allowExtra(true).allowNull(true),
        race_card: type.object().optional().allowExtra(true).allowNull(true),
        verdict: type.object().optional().allowExtra(true).allowNull(true),
        runners: type.object().optional().allowExtra(true).allowNull(true),
        bet_prompts: type.object().optional().allowExtra(true).allowNull(true),
        match_url: type.string().optional(),
        odds_data: type.object().optional().allowExtra(true).allowNull(true),
        spotlight_verdict_selection: type.object().optional().allowExtra(true).allowNull(true),
    },
    {
        conflict: 'replace',
        pk: '_race_id',
    }
)

module.exports = Race
