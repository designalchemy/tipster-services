const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

const Act = thinky.createModel('Act', {
    _act_id: type.string().default(uuid.v4),
    sport_name: type.string(),
    act_scenes: [
        {
            scene_index: type.number(),

            scene_components: [
                {
                    component: type.string(),
                    component_data: type.object().optional().allowExtra(true).allowNull(true)
                }
            ]
        }
    ]
})

module.exports = Act
