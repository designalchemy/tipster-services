const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Group = thinky.createModel('Group', {
    _group_id: type.string().default(uuid.v4),
    group_name: type.string(),
    group_brands_id: type.array(),
})

module.exports = Group

// -------- CUSTOM METHODS -------- //

// // Make it easier to query with joins
// Group.defineStatic('getAll', (id) =>
//     Group
//         .get(id)
//         .getJoin({ brands: true })
//         .run())

// // Attempt to enforce referential integrity.
// Group.defineStatic('deleteAll', (id) =>
//     Group
//         .getAll(id)
//         .then(group => group.deleteAll()))
