const thinky = require('../util/thinky')
const uuid = require('uuid')

const type = thinky.type

// -------- SCHEMA -------- //

const Users = thinky.createModel(
    'Users',
    {
        id: type.string().default(uuid.v4),
        firstName: type.string(),
        secondName: type.string(),
        email: type.string(),
        role: type.string().optional(),
        skype: type.string().optional(),
        password: type.string(),
        organisation: type.string(),
        ip: type.string()
    },
    {
        conflict: 'update',
        pk: 'id'
    }
)

module.exports = Users
