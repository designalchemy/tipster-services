const config = require('config')
const thinky = require('thinky')
const _ = require('lodash')

// By requiring this file we can ensure thinky only initialised once.
const inst = thinky(config.get('rethink'))

const shardDefaults = {
    shards: config.rethink.shards || 1,
    replicas: config.rethink.replicas || 1
}

// Monkeypatching createModel to ensure that tables are replicated:
const originalCreateModelMethod = inst.createModel
inst.createModel = (name, schema, options = {}) => {
    // Someone could pass some value through here, avoid clobbering:
    options.table = _.defaults(options.table, shardDefaults)
    const Model = originalCreateModelMethod.call(inst, name, schema, options)

    // Ensure already existing table has correct number of shards / replicas:
    Model.addListener('ready', () => {
        const tableName = Model.getTableName()
        const tableOptions = _.pick(options.table, ['shards', 'replicas'])
        inst.r.table(tableName).reconfigure(tableOptions).run()
    })

    return Model
}

module.exports = inst
