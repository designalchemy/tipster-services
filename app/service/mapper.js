const request = require('request-promise-native')
const _ = require('lodash')

class Mapper {
    constructor() {
        this.commandQueue = []
        this.lastCatchIndex = -1
        this.currentError = null
    }

    then(resolved) {
        this.commandQueue.push({
            command: 'then',
            data: {
                resolved
            }
        })
        return this
    }

    fetch(requestParams, parser = x => x) {
        // construct our computation
        this.commandQueue.push({
            command: 'fetch',
            data: {
                requestParams,
                parser
            }
        })
        return this
    }

    catch(errorHandler) {
        this.lastCatchIndex = this.commandQueue.length
        this.commandQueue.push({
            command: 'catch',
            data: {
                errorHandler
            }
        })
        return this
    }

    set(key, value) {
        this.commandQueue.push({
            command: 'set',
            data: {
                key,
                value
            }
        })
        return this
    }

    async run() {
        let results = {}
        let i = 0
        // execute our computation
        for (const { command, data } of this.commandQueue) {
            try {
                if (!this.currentError || command === 'catch') {
                    results = await this.commandRunner(command, data, results)
                }
            } catch (e) {
                if (this.lastCatchIndex < i) {
                    const errMsg = this.formatError(command, data, e)
                    throw new Error(errMsg)
                } else {
                    this.currentError = e
                }
            }
            i += 1
        }
        return results
    }

    async commandRunner(commandName, data, results) {
        switch (commandName) {
            case 'fetch': {
                const response = await request.get(data.requestParams)
                const json = JSON.parse(response)
                const parsed = await data.parser(results, json)
                return parsed
            }

            case 'then': {
                return await data.resolved(results)
            }

            case 'set':
                return _.set(results, data.key, data.value)

            case 'catch': {
                const err = this.currentError
                this.currentError = null
                if (err) {
                    return data.errorHandler(err, results)
                }
                return results
            }
            default:
                throw new TypeError(`Unknown command: ${commandName}`)
        }
    }

    formatError(command, data, err) {
        return `Command "${command}" with data ${JSON.stringify(data)}. Message: "${err.message}"`
    }
}

module.exports = Mapper
