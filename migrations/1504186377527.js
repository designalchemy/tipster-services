'use strict'

const r = require('../app/util/thinky').r

exports.up = next => {
    r
        .table('Dashboard')
        .filter(row => {
            return row('config').hasFields('sport').not()
        })
        .update({ config: { sport: '1' } })
        .run()
        .then(() => next())
        .catch(next)
}

exports.down = next => {
    next()
}
