'use strict'
const thinky = require('../app/util/thinky')

const r = thinky.r

exports.up = function(next) {
    r.table('Auth').indexCreate('location')
    r.table('Dashboard').indexCreate('dashboard_brand_id')
    r.table('Config').indexCreate('config_brand_id')
    r.table('Users').indexCreate('organisation')

    next()
}

exports.down = function(next) {
    next()
}
