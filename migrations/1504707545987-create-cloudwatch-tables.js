'use strict'

const thinky = require('../app/util/thinky')
const r = thinky.r
const tablenames = ['cloudwatch_timestamp', 'cloudwatch_logs']

exports.up = async function(next) {
    try {
        const tables = await r.tableList().run()
        for (const tablename of tablenames) {
            if (!tables.includes(tablename)) {
                await r.tableCreate(tablename).run()
            }
        }
        next()
    } catch (e) {
        next(e)
    }
}

exports.down = function(next) {
    try {
        for (const tablename of tablenames) {
            r.tableDrop(tablename).run()
        }
        next()
    } catch (e) {
        next(e)
    }
}
