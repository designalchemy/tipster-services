'use strict'

const thinky = require('../app/util/thinky')

const r = thinky.r

const data = require('../seed_data/Act.json')

exports.up = next => {
    r.table('Act').update(data).run().then(() => next()).catch(next)
}

exports.down = next => {
    next()
}
