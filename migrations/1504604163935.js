'use strict'

const r = require('../app/util/thinky').r

exports.up = next => {
    r
        .tableList()
        .run()
        .then(tables => {
            if (!tables.includes('RaceIndex')) {
                return r.tableCreate('RaceIndex').run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = next => {
    next()
}
