'use strict'

const r = require('../app/util/thinky').r

const data = require('../seed_data/Act.json')

exports.up = next => {
    r.table('Act').replace(data).run().then(() => next()).catch(err => console.log(err))
}

exports.down = next => {
    next()
}
