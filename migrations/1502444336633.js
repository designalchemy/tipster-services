'use strict'

const config = require('config')
const thinky = require('../app/util/thinky')
const r = thinky.r
const shardOptions = {
    shards: config.rethink.shards || 1,
    replicas: config.rethink.replicas || 1
}

exports.up = function(next) {
    // get all tables
    r
        .db(config.rethink.db)
        .tableList()
        .run()
        .then(tables => {
            // loop through them and ensure correct shard / replicas
            const proms = tables.map(table => {
                return r.db(config.rethink.db).table(table).reconfigure(shardOptions).run()
            })
            return Promise.all(proms)
        })
        .then(() => next())
        .catch(err => next(err))
}

exports.down = function(next) {
    next()
}
