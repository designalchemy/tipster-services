'use strict'

const r = require('../app/util/thinky').r

const tablename = 'hz_client_connections'

exports.up = function(next) {
    r
        .tableList()
        .run()
        .then(tables => {
            if (!tables.includes(tablename)) {
                return r.tableCreate(tablename).run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = function(next) {
    r.tableDrop(tablename).run(next)
}
