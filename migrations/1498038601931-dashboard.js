'use strict'

const thinky = require('../app/util/thinky')

const r = thinky.r

const data = require('../seed_data/Dashboard.json')

exports.up = next => {
    r.table('Dashboard').insert(data).run().then(x => next()).catch(next)
}

exports.down = next => {
    next()
}
