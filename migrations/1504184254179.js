'use strict'

const r = require('../app/util/thinky').r

const tableName = 'GH_Odds'

exports.up = next => {
    r
        .tableList()
        .run()
        .then(tables => {
            if (!tables.includes(tableName)) {
                return r.tableCreate(tableName, { primaryKey: 'bookmaker_id' }).run()
            }
        })
        .then(() => next())
        .catch(next)
}

exports.down = next => {
    next()
}
