// Production config.
const composeCert = `-----BEGIN CERTIFICATE-----
MIIDdzCCAl+gAwIBAgIEWQM+6zANBgkqhkiG9w0BAQ0FADA9MTswOQYDVQQDDDJL
b3JlbG9naWMgTGltaXRlZC04MmUxZTI0MWUwODQ1ZWQzNWZjN2RhN2I2MWNhYzc4
OTAeFw0xNzA0MjgxMzA4NTlaFw0zNzA0MjgxMzAwMDBaMD0xOzA5BgNVBAMMMktv
cmVsb2dpYyBMaW1pdGVkLTgyZTFlMjQxZTA4NDVlZDM1ZmM3ZGE3YjYxY2FjNzg5
MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAul+7m5Rs/IVCZ9eZAbOG
soEez+sz1Z6mTGBI+XQj4d67i84iSit6xXvTWjAdILMhlyl2H6TLuYfsSTW0gJPR
XrZx+skavIKkIObrKttbsLSeMMtHc9xW4cKIGkaWF/L4b7ZjAtxVDGceOWXT5PrW
ODvcGkJsyw4dXwZcyy21LUsb9kYKXq1zbAGmK+Li0KhkuhWyKuwoTY+CWMrSae4Z
ZdkcYy9g7e2/pWnlitTnE5uByivi0S+XWl68BXIDauW0XtfqgiVVfxn6JhfxhgQ1
NpqE3aG9T9MtKKoDeoEfnbpCVGjAb1loOc9BNzhRE5ezG2zcVjNmSWrog0dWBZjv
gwIDAQABo38wfTAdBgNVHQ4EFgQUi9DP6S+RnUJGwYBYft2GqUOP7yowDgYDVR0P
AQH/BAQDAgIEMB0GA1UdJQQWMBQGCCsGAQUFBwMBBggrBgEFBQcDAjAMBgNVHRME
BTADAQH/MB8GA1UdIwQYMBaAFIvQz+kvkZ1CRsGAWH7dhqlDj+8qMA0GCSqGSIb3
DQEBDQUAA4IBAQCAqDaB03/nYHqQHdHnxNtxvkulyFGRmYAiYQFGiAumiCP2/4DD
IutYibsqgYsfcWq1b+beQGg/gjNKbruul8V5cg8YFWijOBwhb7+9SyQM4biKu2FB
GQOe+90Qsp907X2sR+zS0S2VujgtodLy8Axuq31fKYU+JSSTycQZqamOqCDZIFrN
Ke9UjjtOLnfXs3J7l4gEtF9jIpkfyg9I8UNvkrVkni7HLyeG1AJ9Tduau/+E4zlp
yLVL0gpADo8KYcgG+Fc+Bqr9VlZXt/tpYVXDhOynU05PzV/OuvTbD7NSYxQPjPBi
sD4e4r0PiNYQsV3BWqSvZQYosjxIqBAOPMSb
-----END CERTIFICATE-----`

// We need the database credentials for horizon and thinky to be the same...
const db = {
    name: process.env.RETHINK_NAME,
    port: process.env.RETHINK_PORT,
    host: process.env.RETHINK_HOST,
    password: process.env.RETHINK_PASSWORD
}

module.exports = {
    // 8181 is the default port that the horizon client listens to.
    port: process.env.PORT,

    // [error,warn,info,verbose,debug,silly]
    loggingLevel: 'info',

    // Horizon configuration (http://horizon.io/docs/embed/).
    horizon: {
        project_name: db.name,
        rdb_host: db.host,
        rdb_port: db.port,
        rdb_password: db.password,
        rdb_ssl: { ca: [composeCert] },
        auth: {
            token_secret: process.env.HORIZON_SECRET,
            duration: '1d',
            create_new_users: true,
            new_user_group: 'authenticated',
            allow_anonymous: true,
            allow_unauthenticated: true
        },
        auto_create_collection: true,
        auto_create_index: true,
        permissions: false,
        path: '/horizon'
    },

    rethink: {
        host: db.host,
        port: db.port,
        db: db.name,
        password: db.password,
        ssl: { ca: [composeCert] },

        shards: 1,
        // It's impossible to have more replicas of the data than there are servers!
        // This is set to work with compose.
        replicas: 3,

        // Deprecated options .. though not sure what in favour of :-S
        enforce_missing: true,
        enforce_extra: 'strict',
        enforce_type: 'strict'
    },

    webpack: 'production'
}
