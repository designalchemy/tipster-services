// Test config.

// We need the database credentials for horizon and thinky to be the same...
const db = {
    name: 'tipster__testing',
    port: 28015,
    host: 'localhost',
}

module.exports = {
    // 8181 is the default port that the horizon client listens to.
    port: 8181,

    // [error,warn,info,verbose,debug,silly]
    loggingLevel: 'debug',

    // Horizon configuration (http://horizon.io/docs/embed/).
    horizon: {
        project_name: db.name,
        rdb_host: db.host,
        rdb_port: db.port,
        auth: {
            token_secret: 'secret',
            duration: '1d',
            create_new_users: true,
            new_user_group: 'authenticated',
            allow_anonymous: true,
            allow_unauthenticated: true,
        },
        auto_create_collection: true,
        auto_create_index: true,
        permissions: true,
        path: '/horizon',
    },

    rethink: {
        host: db.host,
        port: db.port,
        db: db.name,

        // Deprecated options .. though not sure what in favour of :-S
        enforce_missing: true,
        enforce_extra: 'strict',
        enforce_type: 'strict',
    },
}
