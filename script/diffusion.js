const diffusion = require('diffusion')
const moment = require('moment')
const _ = require('lodash')
const Xregex = require('xregexp')
const thinky = require('../app/util/thinky')
const fetch = require('./fetcher')
const ThrottledQueue = require('./ThrottledQueue')

const log = require('../common/logger')('diffusion')

const r = thinky.r

const argv = require('minimist')(process.argv.slice(2))

let session = null

const jobQueue = new ThrottledQueue(50)

async function main() {
    if (session) session.close()

    const maximumTimeoutDuration = 1000 * 60 * 10
    const maximumAttemptInterval = 1000 * 60
    const maximumAttempts = 25
    let attempts = 0

    // https://docs.pushtechnology.com/docs/5.8.3/manual/html/developerguide/client/basics/reconnecting_strategy.html
    const reconnectionStrategy = (start, abort) => {
        log.debug('Try to reconnect')

        if (attempts > maximumAttempts) {
            abort()
        } else {
            const wait = Math.min(Math.pow(2, attempts++) * 100, maximumAttemptInterval)
            setTimeout(start, wait)
        }
    }

    const updateDb = (url, bookmaker_id, runner_name, odds, running, sport) => {
        const table = sport === 'HORSES' ? 'Odds' : 'GH_Odds'

        jobQueue.add(() => {
            log.info(`${sport} ${url} ${runner_name} ${odds} ${running} ${bookmaker_id}`)

            const dataToSave = bookmaker_id === '#INFO' ? { running: running } : { odds: odds }
            const keyValue = _.keys(dataToSave)[0]

            return r
                .table(table)
                .insert(
                    {
                        bookmaker_id: bookmaker_id,
                        odds_data: {
                            [url]: {
                                [runner_name]: {
                                    [keyValue]: dataToSave[keyValue]
                                }
                            }
                        }
                    },
                    { conflict: 'update' }
                )
                .run()
                .catch(err => log.error(err))
        })
    }

    const parseLeafCategory = val => {
        const resultArray = val.toString().split('\u0002')
        const keys = [
            'rp_outcome_uid',
            'bk_outcome_uid',
            'bk_sp_uid',
            'bk_price_uid',
            'bk_event_uid',
            'bk_meeting_uid',
            'status',
            'odds',
            'decimal',
            'hist_f1',
            'hist_f2',
            'hist_f3'
        ]
        return _.zipObject(keys, resultArray)
    }

    const getDashboard = await r.table('Dashboard').run()
    const sportsBeingUsed = _.uniq(_.map(getDashboard, item => item.config.sport))

    log.info('sportsBeingUsed', sportsBeingUsed)

    try {
        session = await diffusion.connect({
            // host: 'push-dev.racingpost.com',
            // port: 80,
            host: 'push-retail.racingpost.com',
            port: 443,
            secture: 'true',
            reconnect: {
                timeout: maximumTimeoutDuration,
                strategy: reconnectionStrategy
            }
        })

        log.debug('Connected to diffusion')

        session.on({
            disconnect: () => {
                log.warn('Diffusion session disconnected')
            },
            reconnect: () => {
                log.warn('Diffusion session reconnected!')
                attempts = 0
            },
            error: error => {
                log.error(error)
            },
            close: reason => {
                log.warn(`Diffusion session closed: ${reason}`)
            }
        })

        const todayDate = moment().format('YYYY-MM-DD')

        const bookMakers = [
            '#INFO',
            '#BESTODDS',
            'WH_OXI',
            'VC',
            'SPORTING INDEX',
            'PPWR',
            'RB',
            'CORA',
            'BOLEYSPORTS',
            'SURREY',
            'BET365',
            'SUPERSOCCER',
            'EB',
            'SIS'
        ]

        // make array of all types of topics to subscribe to
        const odds = _.flatMap(sportsBeingUsed, item => {
            if (item === '1') {
                // if horse racing
                return _.map(bookMakers, item => {
                    return session
                        .subscribe(`?HORSES/${todayDate}/.*/.*/WIN/.*/${item}`)
                        .transform(parseLeafCategory)
                })
            } else if (item === '2') {
                // if grayhounds
                return _.map(bookMakers, item => {
                    return session
                        .subscribe(`?GREYHOUNDS/${todayDate}/.*/.*/OUTRIGHT WINNER/.*/${item}`)
                        .transform(parseLeafCategory)
                })
            }
        })

        // parse the topic into a useable object
        const topicParser = Xregex(
            `(?<sport> .*)/(?<date> .*)/(?<course_name> .*)/(?<time> .*)/(?<market> .*)/(?<runner_name> .*)/(?<bookmaker> .*)`,
            'x'
        )

        _.forEach(odds, item => {
            item.on('update', (value, topic) => {
                const match = Xregex.exec(topic, topicParser)
                const { sport, time, runner_name, bookmaker } = match
                let { course_name } = match

                if (course_name === 'NEWMARKET <O>JULY<C>') {
                    course_name = 'NEWMARKET'
                }

                const oddsData = value.odds
                const running = value.bk_outcome_uid
                const url = `${todayDate}/${course_name}/${time}`

                updateDb(url, bookmaker, runner_name, oddsData, running, sport)

                if (running === 'N') {
                    log.info('I want new predictor data for', url)
                    fetch.queueUpdatePredictor(url, runner_name)
                }
            })

            item.on('error', err => {
                log.error(err)
            })
        })
    } catch (error) {
        // Failed to connect
        log.error(error)
        main()
    }
}

if (require.main === module) {
    main()
        .then(() => log.debug('Process completed.'))
        .catch(err => log.error(err))
}

module.exports = main
