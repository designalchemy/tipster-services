const promisify = require('pify')
const _ = require('lodash')
const Parser = require('./logParser')
const { URL } = require('url')
const uuidValidate = require('uuid-validate')
const assert = require('assert')
const r = require('../app/util/thinky').r
const log = require('../common/logger')('cloudwatch')
const config = require('config')

const AWS = require('aws-sdk')

// Username              tipster-app-server
// Password
// Access key ID         AKIAI5KPLQW73LP7RILQ
// Secret access key     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
// Console login link    https://692243509112.signin.aws.amazon.com/console

const cw = new AWS.CloudWatchLogs({
    region: 'eu-west-1',
    username: 'tipster-app-server',
    password: '',
    accessKeyId: 'AKIAI5KPLQW73LP7RILQ',
    secretAccessKey: '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'
})

// Promisify functions:
const describeLogStreams = promisify(cw.describeLogStreams).bind(cw)
const getLogEvents = promisify(cw.getLogEvents).bind(cw)

const env = config.env === 'production' ? 'prod' : 'staging'

const parseConfigId = url => {
    try {
        const end = new URL(url).pathname.split('/').pop()
        const isValid = uuidValidate(end) || end === '1'
        assert(isValid, 'Requires valid uuid')
        return end
    } catch (e) {
        return '_'
    }
}

const roundTime = (date, increment) =>
    new Date(Math.round((date.getTime() - increment / 2) / increment) * increment)

const dateToClosestMinute = () => {
    const now = new Date()
    const minute = 60 * 1000
    return roundTime(now, minute)
}

const collectEventMessages = async (logGroupName, logStreams, endTime, startTime) => {
    let eventMessages = []

    for (const { logStreamName } of logStreams) {
        const { events } = await getLogEvents({
            logStreamName,
            logGroupName,
            endTime: endTime.getTime(),
            startTime: startTime.getTime()
        })
        eventMessages = eventMessages.concat(_.map(events, e => e.message))
    }

    return eventMessages
}

const fetchTimestamp = async () => {
    const lastUpdated = await r
        .table('cloudwatch_timestamp')
        .get(1)
        .run()

    return lastUpdated ? lastUpdated.timestamp : new Date(0)
}

const setTimestamp = timestamp =>
    r
        .table('cloudwatch_timestamp')
        .insert({ id: 1, timestamp }, { conflict: 'replace' })
        .run()

// Parse messages and aggregate all data by device and config ID. Creates a
// nested object with the following structure:
//
// {
//     'some-device-id': {
//         'some-config-id': { total: 123, count: 1 },
//         'another-config-id': { total: 456, count: 2 },
//     }
// }
const aggregate = (eventMessages, timestamp) =>
    _.reduce(
        eventMessages,
        (coll, message) => {
            try {
                // Parse messages
                const result = Parser.Message.tryParse(message)
                const deviceId = result.deviceId || '_'
                const configId = parseConfigId(result.url)

                // add bytes
                const currentTotal = _.get(coll, [deviceId, configId, 'total']) || 0
                const newTotal = currentTotal + result.transferred

                // increment count
                const currentCount = _.get(coll, [deviceId, configId, 'count']) || 0
                const newCount = currentCount + 1

                // Have to use `setWith` because sometimes using `set` resulted in
                // creation of arrays instead of objects if key is numerical.
                return _.setWith(
                    coll,
                    [deviceId, configId],
                    { total: newTotal, count: newCount, timestamp },
                    // Specify that new structure is always an object:
                    Object
                )
            } catch (e) {
                log.error(
                    `Parsing error:\n\n   ${e.message}\n\nwhen parsing message:\n\n   ${message}`
                )
                return coll
            }
        },
        {}
    )

// Normalise aggregated data into a flat array of objects in order to
// easily insert them into the DB.
//
// {
//     'some-device-id': {
//         'some-config-id': { total: 123, count: 1, timestamp: ... },
//         'another-config-id': { total: 456, count: 2, timestamp: ... },
//     }
// }
//
// ... is transformed into ...
//
// [
//     {
//         deviceId: 'some-device-id',
//         configId: 'some-config-id',
//         total: 123,
//         count: 1,
//         timestamp: ...
//     },
//     {
//         deviceId: 'some-device-id',
//         configId: 'another-config-id',
//         total: 456,
//         count: 2,
//         timestamp: ...
//     },
// ]
const normalise = aggregatedGroups =>
    _.flatMap(
        aggregatedGroups,
        (group, deviceId) =>
            _.map(group, ({ total, count, timestamp }, configId) => ({
                deviceId,
                configId,
                total,
                count,
                timestamp
            })),
        []
    )

const saveLogs = logs =>
    r
        .table('cloudwatch_logs')
        .insert(logs)
        .run()

async function main() {
    const startTime = await fetchTimestamp()
    const endTime = dateToClosestMinute()
    await setTimestamp(endTime)

    const logGroupName = `/aws/elasticbeanstalk/tipster-${env}-web/var/log/nginx/access.log`
    const { logStreams } = await describeLogStreams({ logGroupName })
    const eventMessages = await collectEventMessages(logGroupName, logStreams, endTime, startTime)

    const aggregatedGroups = aggregate(eventMessages, endTime)
    const records = normalise(aggregatedGroups)

    await saveLogs(records)

    log.info(`Updated ${records.length} records`)
}

module.exports = main
