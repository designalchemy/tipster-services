const thinky = require('../app/util/thinky')
const _ = require('lodash')

const r = thinky.r

module.exports = async () => {
    console.log('//////////////////////////////////////////////////')
    console.log('CLEANING IPS!!')
    console.log('//////////////////////////////////////////////////')

    const allAuth = await r
        .table('Auth')
        .filter({ ip_whitelist: true })
        .run()
        .then(resp =>
            _.sortBy(
                _.flatMap(resp, item =>
                    _.map(item.configId, cfg =>
                        _.assign({}, item, { configRef: cfg.id }, { configAuth: cfg.auth })
                    )
                )
            )
        )

    const ids = _.map(allAuth, o => o.id)

    const matchedWithUUIds = await r
        .table('hz_client_connections')
        .getAll(...ids, { index: 'uuid' })
        .run()
        .then(resp => resp)

    const allOnline = await _.filter(matchedWithUUIds, item => !item.offline)

    await _.forEach(allOnline, o => {
        _.remove(matchedWithUUIds, off => o.uuid === off.uuid && o.page === off.page)
    })

    const offlines = await _.reduce(
        matchedWithUUIds,
        (acc, o) => {
            const offline = {
                uuid: o.uuid,
                page: o.page
            }
            acc.push(offline)
            return acc
        },
        []
    )

    const allOffline = await _.uniqWith(offlines, _.isEqual)

    await _.forEach(allOffline, off => {
        r
            .table('Auth')
            .get(off.uuid)
            .run()
            .then(auth => {
                _.remove(auth.configId, o => o.id === off.page)
                r
                    .table('Auth')
                    .get(off.uuid)
                    .update(auth)
                    .run()
            })
    })
}
