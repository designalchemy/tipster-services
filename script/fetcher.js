const Mapper = require('../app/service/mapper')
const Dashboard = require('../app/model/Dashboard')
const Race = require('../app/model/Race')
const _ = require('lodash')
const fp = require('lodash/fp')
const chalk = require('chalk')
const thinky = require('../app/util/thinky')
var moment = require('moment-timezone')

moment.tz('Europe/London')

const log = require('../common/logger')('fetcher')

const r = thinky.r
// const api = 'http://p1-api.rp-dev.com/horses'
// const api = 'https://hox082vzk2.execute-api.eu-west-1.amazonaws.com/stage/horses'
const api = 'https://api.rpb2b.io/horses'

const apiOptions = endpoint => ({
    uri: `${api}${endpoint}`,
    headers: {
        'x-api-key': 'LmV6xOZFi45An9Tq094tiaimmmp7hvw97qmb5hig',
        'x-jwt-key':
            'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpaWEpwWVd3aU9pSXlNREUzTURZeU56RTBNakl4TXpZMk15SXNJblpsY25OcGIyNGlPaUppTW1JaUxDSmljbUZ1WTJnaU9pSXdJaXdpY0dGeWRHNWxjbDlqYjJSbElqb2lURzFXTm5oUFdrWnBORFZCYmpsVWNUQTVOSFJwWVdsdGJXMXdOMmgyZHprM2NXMWlOV2hwWnlKOS44OHNpV0wySDMxcE1nS1paZk1CenI4OUFFM0Z2ZU5LX2dGWExMX0xPQmpB'
    }
})

const getPredictorData = async (acc, json, raceId, mappedBets) => {
    if (json.status === 404) {
        // if no predictor
        log.debug('no predictor')
        return _.set(acc, 'predictor', { empty: true })
    }

    const runners = json.data.runners
    const bets = mappedBets

    if (_.isNil(bets[raceId])) {
        // if no bet data for the predictor
        return _.set(acc, 'predictor', json.data)
    }

    for (const horse_id of Object.keys(runners)) {
        const runner = runners[horse_id]
        let silkColor = 'red'

        if (bets[raceId] && bets[raceId][horse_id]) {
            silkColor = _.find(
                [
                    'red',
                    'orange',
                    'yellow',
                    'green',
                    'blue',
                    'purple',
                    'pink',
                    'black',
                    'white',
                    'brown'
                ],
                color => bets[raceId][horse_id]['silk_' + color]
            )
        }

        runner.color = silkColor
    }

    return _.set(acc, 'predictor', json.data)
}

async function updatePredictor(url, horseName) {
    const res = await r
        .table('Race')
        .filter({ match_url: url })
        .run()
    // const data = _.head(res) ? _.head(res).predictor.runners : ''
    const raceId = _.get(res, [0, '_race_id'])
    // const findHorse = _.find(data, { diffusion_name: _.capitalize(horseName) })

    if (!raceId) {
        throw new Error('No Race ID on update predictor from diffusion')
    }

    const raceBets = await new Mapper()
        .fetch(apiOptions('/bet-finder'), (acc, json) => json.data.bets)
        .run()
        .catch(err => log.error(err))

    const mappedBets = filterRaces(raceBets)

    const predictor = await new Mapper()
        .fetch(apiOptions(`/predictor/${raceId}`), (acc, json) =>
            getPredictorData(acc, json, raceId, mappedBets)
        )
        .run()
        .catch(err => log.error(err))

    const msg = await r
        .table('Race')
        .get(raceId)
        .update({ predictor: r.literal(predictor.predictor) })
        .run()
        .catch(err => log.error(err))

    log.debug(`Updated predictor via diffusion: ${JSON.stringify(msg)}`)
}

const updatePredictorQueue = []
function queueUpdatePredictor(url, horseName) {
    updatePredictorQueue.push({ url, horseName })
}

const pause = ms =>
    new Promise((resolve, reject) => {
        setTimeout(resolve, ms)
    })

const loop = async function() {
    if (updatePredictorQueue.length > 0) {
        const { url, horseName } = updatePredictorQueue.shift()
        await updatePredictor(url, horseName).catch(e => log.error(e))
    } else {
        await pause(100)
    }
    loop()
}

loop()

async function getRaces() {
    const failed = []

    log.debug('Fetching upcoming races')

    const allowedLocations = ['GB', 'IRE']

    const raceIds = await new Mapper() // make list of all races IDs
        .fetch(apiOptions(`/racecards/date/${moment().format('YYYY-MM-DD')}`), (acc, json) =>
            _.flatMap(json.data.list, list => _.map(list.races, race => race.race_instance_uid))
        )
        .run()

    const raceBets = await new Mapper()
        .fetch(apiOptions('/bet-finder'), (acc, json) => json.data.bets)
        .run()

    const mappedBets = filterRaces(raceBets)

    let i = 0

    for (const raceId of raceIds) {
        i++
        const numOf = `${i}/${raceIds.length}`

        try {
            log.info(`(${numOf}) Fetching race: ${raceId}.`)

            const raceObject = await new Mapper()
                .fetch(apiOptions(`/predictor/${raceId}`), (acc, json) =>
                    getPredictorData(acc, json, raceId, mappedBets)
                )
                .fetch(apiOptions(`/racecards/${raceId}`), (acc, json) => {
                    const date = moment(json.data.race_card.race_datetime).format('YYYY-MM-DD')
                    const time = moment(json.data.race_card.race_datetime)
                        .tz('Europe/London')
                        .format('HH:mm')

                    const filterLocation = json.data.race_card.course_name
                        .replace(/ \(.*?\)/g, '')
                        .trim()

                    log.info(`${date}/${filterLocation}/${time}`)

                    if (json.data.race_card.foreign) {
                        throw new Error('Foreign Race - dont save this one')
                    }

                    if (json.data.race_card.race_type_code === 'P') {
                        throw new Error('Point To Point Race - dont save this one')
                    }

                    acc.match_url = `${date}/${filterLocation}/${time}`

                    return _.merge(acc, json.data)
                })
                .fetch(apiOptions(`/racecards/verdict/${raceId}`), (acc, json) => {
                    if (
                        !_.includes(allowedLocations, _.head(json.data.verdict).course_country_code)
                    ) {
                        throw new Error('Foreign Race - dont save this one')
                    }

                    return _.merge(acc, {
                        verdict: _.first(json.data.verdict),
                        spotlight_verdict_selection: json.data.spotlight_verdict_selection
                    })
                })
                .fetch(apiOptions(`/racecards/runners/${raceId}`), (acc, json) =>
                    _.merge(acc, json.data)
                )
                .fetch(apiOptions(`/bet-prompts/${raceId}/10`), (acc, json) => {
                    if (!_.includes(allowedLocations, json.data.bet_prompts.country_code)) {
                        throw new Error('Foreign Race - dont save this one')
                    }

                    return _.merge(acc, json.data)
                })
                .set('_race_id', String(raceId))
                .run()

            log.info(`(${numOf}) Saving race: ${raceId}`)
            Race.save(raceObject, { conflict: 'replace' })
        } catch (e) {
            log.error(e.message)
            failed.push(raceId)
        }
    }

    log.info('Adding races to dashboard')

    const dashboards = await r
        .table('RaceIndex')
        .insert({ id: 'horses', allIds: raceIds })
        .run()
        .catch(err => log.error(err))

    return failed
}

const filterRaces = (races, racesId) =>
    _.reduce(races, (acc, race, i) => _.set(acc, [race.race_uid, race.horse_uid], race), {})

if (require.main === module) {
    getRaces()
        .then(failed => {
            if (failed.length) {
                log.warn(
                    `Finished process, but failed with the following races: ${failed.join(', ')}`
                )
                process.exit(2)
            } else {
                log.info('Complete')
                process.exit()
            }
        })
        .catch(e => {
            log.error(e.message)
            process.exit(1)
        })
}

module.exports = { getRaces, queueUpdatePredictor }
