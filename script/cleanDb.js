const thinky = require('../app/util/thinky')

const r = thinky.r

async function cleanDb() {
    r.table('Odds').delete().run()
    r.table('GH_Odds').delete().run()
    r.table('Race').delete().run()
    r.table('GH_Race').delete().run()
}

if (require.main === module) {
    cleanDb().then(() => console.log('fin')).catch(e => console.log(e, 'error, broken'))
}

module.exports = cleanDb
