// Docs for the Parsimmon parsing library can be found at
// https://github.com/jneen/parsimmon/blob/master/API.md
const P = require('parsimmon')
const { alt, digits, fail, letters, optWhitespace, regexp, seq, seqObj, string, succeed } = P

// Defining language:
module.exports = P.createLanguage({
    // Utility definitions:
    _: () => optWhitespace,
    colon: () => string(':'),
    dash: () => string('-'),
    dot: () => string('.'),
    empty: () => string(''),
    minus: () => string('-'),
    plus: () => string('+'),
    quote: () => string('"'),
    slash: () => string('/'),
    quotedText: () => regexp(/"(.*?)"/, 1),

    // example message:
    // 159.203.182.22 - D4B1872D-3B04-454E-8832-3D6A5FE86D61 [18/Aug/2017:14:07:04 +0000] "GET /gantry/ HTTP/1.1" 200 19116 "https://tipster.racingpost.com/gantry" "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/98 Safari/537.4 (StatusCake)"
    Message({
        dash,
        DeviceId,
        IpAddress,
        quote,
        quotedText,
        Request,
        Status,
        TimeStamp,
        Transferred,
        _
    }) {
        return seqObj(
            ['ipAddress', IpAddress],
            _,
            dash,
            _,
            ['deviceId', DeviceId],
            _,
            ['timestamp', TimeStamp],
            _,
            ['request', Request],
            _,
            ['statusCode', Status],
            _,
            ['transferred', Transferred],
            _,
            ['url', quotedText],
            _,
            ['client', quotedText]
        )
    },

    DeviceId() {
        return regexp(/[^\s]*/).map(val => (val === 'unauthenticated' ? null : val))
    },

    Status() {
        return digits.map(Number)
    },

    Transferred() {
        return digits.map(Number)
    },

    /**
     * Request Parser
     *
     * Parses Request String
     *
     * For example:
     *
     *    "GET http://54.171.59.237:80/mysql/admin/ HTTP/1.0"
     *
     * @yields { method: String, protocol: String, url: String }
     */
    Request({ quote, _ }) {
        const method = regexp(/[A-Z]*/)

        const url = regexp(/[^\s]*/)
        const protocol = regexp(/[^\s\"]*/)

        return seqObj(['method', method], _, ['resource', url], _, ['protocol', protocol]).wrap(
            quote,
            quote
        )
    },

    /**
     * IP Address Parser
     *
     * Parses valid IP4 Addresss
     *
     * For example:
     *
     *      85.105.29.241
     *
     * @yields {String}
     */
    IpAddress({ dot }) {
        const ipAddressDigits = digits.chain(
            x => (x <= 255 ? succeed(x) : fail('IP Digits must be below 255'))
        )

        return seq(
            ipAddressDigits,
            dot,
            ipAddressDigits,
            dot,
            ipAddressDigits,
            dot,
            ipAddressDigits
        ).tie()
    },

    /**
     * Timestamp Parser
     *
     * Parses timestamps in the form:
     *
     *      [27/Jul/2017:12:43:27 +0000]
     *
     * @yields {Date}
     */
    TimeStamp({ colon, empty, minus, plus, slash, _ }) {
        const numberPrefix = plus.or(minus).fallback(empty)

        // day / month / year
        const date = seq(digits, slash, letters, slash, digits)

        // hour : minutes : seconds
        const time = seq(digits, colon, digits, colon, digits)

        const offset = numberPrefix.then(digits)

        return seq(date, colon, time, _, offset)
            .map(Date)
            .wrap(string('['), string(']'))
    }
})
