const config = require('config')
const _ = require('lodash')
const requireDir = require('require-dir')
const seedData = requireDir('../seed_data')
const thinky = require('../app/util/thinky')
const models = requireDir('../app/model')

const readline = require('readline')

const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const chalk = require('chalk')

const red = chalk.red
const yellow = chalk.yellow
const green = chalk.green
const gray = chalk.gray

process.on('exit', () => {
    console.log(
        `
    Goodbye.
  `
    )
})

// check is acceptable environment!
// const env = config.util.getEnv('NODE_ENV')
// if (env !== 'test' && env !== 'develop') {
//     console.log(
//         `
// Can only seed "develop" and "test" environments.
//
// Your current environment is "${env}".
//   `
//     )
//
//     process.exit(1)
// }

const dropDb = async function() {
    console.log(green('Clearing Database:'))
    const tables = await thinky.r.db(config.rethink.db).tableList()
    const promises = _.map(tables, dropTable)
    await Promise.all(promises)
}

const seedDb = async function() {
    console.log(green('Seeding Data:'))
    const promises = _.map(seedData, saveTable)
    await Promise.all(promises)
}

const dropTable = tableName => {
    console.log(`Dropping table ${tableName}`)
    return thinky.r.table(tableName).delete()
}

const saveTable = (dataset, tablename) => {
    const Ctor = models[tablename]
    console.log(`Adding data to ${tablename} table`)
    return Ctor.save(dataset)
}

const consult = question =>
    new Promise((resolve, reject) => {
        rl.question(question, answer => {
            resolve(answer.match(/^y(es)?$/i))
        })
    })

async function main() {
    const accepted = await consult(
        `
${yellow('Are you sure that you want to seed the database? y/N:')}
${gray('[hint: this will delete all your data]')}
`
    )
    if (!accepted) {
        throw new Error('User terminated seeding.')
    }
    await dropDb()
    await seedDb()
    console.log(green('Complete'))
}

main()
    .then(() => {
        process.exit(0)
    })
    .catch(err => {
        console.log(
            `
${red(err.message)}
  `
        )
        process.exit(1)
    })
