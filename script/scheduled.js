const cron = require('cron')

const fetch = require('./fetcher')
const gh_fetch = require('./gh_fetcher')
const diffusion = require('./diffusion')
const cleanDb = require('./cleanDb')
const cloudwatch = require('./cloudwatch')
const cleanupClientConnectionsTable = require('./cleanupClientConnectionsTable')
const cleanupClientConnectionsIPTable = require('./cleanupClientConnectionsIPTable')
const log = require('../common/logger')('scheduler')

const attempt = (fn, ...args) => {
    try {
        const result = fn(...args)
        if (result.catch) {
            return result.catch(e => log.error(e.message))
        }
        return result
    } catch (e) {
        log.error(e.message)
    }
}

const fetcherJob = new cron.CronJob({
    // Run every day at 4:00am.
    cronTime: '00 00 04 * * *',
    timeZone: 'Europe/London',
    onTick: async () => {
        await attempt(cleanDb)
        await attempt(fetch.getRaces)
        await attempt(gh_fetch.getRaces)
        await attempt(diffusion)
    },
    // Run immediately.
    runOnInit: true
})

const cleanUpJob = new cron.CronJob({
    // Run every 20 minutes
    cronTime: '00 */20 * * *',
    timeZone: 'Europe/London',
    onTick: async () => {
        await attempt(cleanupClientConnectionsTable)
    },
    // Run immediately.
    runOnInit: true
})

const cleanUpJobIPs = new cron.CronJob({
    // Run every 5 minutes
    cronTime: '*/5 * * * *',
    timeZone: 'Europe/London',
    onTick: async () => {
        await attempt(cleanupClientConnectionsIPTable)
    },
    // Run immediately.
    runOnInit: true
})

const cloudwatchJob = new cron.CronJob({
    // Run every minute.
    cronTime: '00 * * * * *',
    timeZone: 'Europe/London',
    onTick: async () => attempt(cloudwatch),
    // Run immediately.
    runOnInit: true
})

fetcherJob.start()
cleanUpJob.start()
cleanUpJobIPs.start()
cloudwatchJob.start()
