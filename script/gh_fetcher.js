const Mapper = require('../app/service/mapper')
const Dashboard = require('../app/model/Dashboard')
const GhRace = require('../app/model/GH_Race')
const _ = require('lodash')
const fp = require('lodash/fp')
const chalk = require('chalk')
const thinky = require('../app/util/thinky')
var moment = require('moment-timezone')

moment.tz('Europe/London')

const log = require('../common/logger')('fetcher')

const r = thinky.r
// const api = 'http://p1-api.rp-dev.com/greyhounds' // dev
const api = 'https://api.rpb2b.io/greyhounds' // prod

const apiOptions = endpoint => ({
    uri: `${api}${endpoint}`,
    headers: {
        'x-api-key': 'LmV6xOZFi45An9Tq094tiaimmmp7hvw97qmb5hig',
        'x-jwt-key':
            'ZXlKaGJHY2lPaUpJVXpJMU5pSXNJblI1Y0NJNklrcFhWQ0o5LmV5SnpaWEpwWVd3aU9pSXlNREUzTURZeU56RTBNakl4TXpZMk15SXNJblpsY25OcGIyNGlPaUppTW1JaUxDSmljbUZ1WTJnaU9pSXdJaXdpY0dGeWRHNWxjbDlqYjJSbElqb2lURzFXTm5oUFdrWnBORFZCYmpsVWNUQTVOSFJwWVdsdGJXMXdOMmgyZHprM2NXMWlOV2hwWnlKOS44OHNpV0wySDMxcE1nS1paZk1CenI4OUFFM0Z2ZU5LX2dGWExMX0xPQmpB'
    }
})

async function getRaces() {
    const failed = []

    log.debug('Fetching upcoming races')

    const raceIds = await new Mapper()
        .fetch(apiOptions(`/meeting/list/${moment().format('YYYY-MM-DD')}`), (acc, json) => {
            const raceList = _.flatMap(json.data.meeting_list, list => {
                return _.map(list.races, race => {
                    return race.race_instance_uid
                })
            })
            return raceList
        })
        .run()

    let i = 0

    for (const raceId of raceIds) {
        i++
        const numOf = `${i}/${raceIds.length}`

        try {
            log.info(`(${numOf}) Fetching race: ${raceId}.`)

            const raceObject = await new Mapper()
                .fetch(
                    apiOptions(`/meeting/race-card/${raceId}/${moment().format('YYYY-MM-DD')}`),
                    (acc, json) => {
                        const date = moment(json.data.race_card.race_info.race_datetime).format(
                            'YYYY-MM-DD'
                        )
                        const time = moment(json.data.race_card.race_info.race_datetime)
                            .tz('Europe/London')
                            .format('HH:mm')

                        const filterLocation = json.data.race_card.race_info.track_name
                            .replace(/ \(.*?\)/g, '')
                            .trim()

                        log.info(`${date}/${filterLocation}/${time}`)

                        acc.match_url = `${date}/${filterLocation}/${time}`
                        acc.race_card = json.data.race_card.race_info
                        acc.runners = json.data.race_card.runners

                        return acc
                    }
                )
                .fetch(apiOptions(`/bet-prompts/${raceId}`), (acc, json) => {
                    delete json.data.race_details
                    return _.merge(acc, { bet_prompts: json.data })
                })
                .fetch(apiOptions(`/predictor/${raceId}`), (acc, json) => _.merge(acc, json.data))
                .catch((err, acc) => {
                    console.log(err)
                    return acc
                })
                .fetch(apiOptions(`/post-picks/race/${raceId}`), (acc, json) =>
                    _.merge(acc, json.data)
                )
                .fetch(apiOptions(`/card/statistics/${raceId}`), (acc, json) =>
                    _.merge(acc, json.data)
                )
                .fetch(apiOptions(`/trap-selections/${raceId}`), (acc, json) =>
                    _.merge(acc, json.data)
                )
                .set('_race_id', String(raceId))
                .run()

            log.info(`(${numOf}) Saving race: ${raceId}`)
            GhRace.save(raceObject, { conflict: 'replace' })
        } catch (e) {
            log.error(e.message)
            failed.push(raceId)
        }
    }

    console.log(raceIds)

    const dashboards = await r
        .table('RaceIndex')
        .insert({ id: 'grayhound', allIds: raceIds })
        .run()
        .catch(err => log.error(err))
}

// const filterRaces = (races, racesId) =>
//     _.reduce(races, (acc, race, i) => _.set(acc, [race.race_uid, race.horse_uid], race), {})

if (require.main === module) {
    getRaces()
        .then(failed => {
            if (failed) {
                log.warn(
                    `Finished process, but failed with the following races: ${failed.join(', ')}`
                )
                process.exit(2)
            } else {
                log.info('Complete')
                process.exit()
            }
        })
        .catch(e => {
            log.error(e.message)
            process.exit(1)
        })
}

module.exports = { getRaces }
