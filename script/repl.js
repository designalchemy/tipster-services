const repl = require('repl')
const requireDir = require('require-dir')

const models = requireDir('../app/model')
const _ = require('lodash')

const r = repl.start({
    prompt: 'tipster-backend: ',
})

r.on('exit', () => {
    console.log('Bye')
    process.exit()
})

_.each(models, (model, name) => {
    Object.defineProperty(r.context, name, {
        configurable: false,
        enumerable: true,
        value: model,
    })
})

const listModels = () => _.keys(models)

Object.defineProperty(r.context, 'listModels', {
    configurable: false,
    enumerable: true,
    value: listModels,
})
