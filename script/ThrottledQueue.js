class ThrottledQueue {
    constructor(timeout = 100, log = console) {
        this._queued = []
        this._timeout = timeout
        this._loop()
        this._log = log
    }

    _loop() {
        setTimeout(() => {
            const action = this._queued.shift()

            // Nothing in queue. Loop.
            if (action) {
                const val = action()

                // If it is a promise, allow to resolve before looping again.
                if (val && val.then) {
                    return val.then(() => this._loop()).catch(err => {
                        this._log.error(err)
                        this._loop()
                    })
                }
            }

            // Otherwise executei loop immediately
            this._loop()
        }, this._timeout)
    }

    add(fn) {
        this._queued.push(fn)
    }
}

module.exports = ThrottledQueue
