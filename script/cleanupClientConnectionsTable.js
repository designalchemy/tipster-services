const thinky = require('../app/util/thinky')
const r = thinky.r

module.exports = async () => {
    const now = new Date()
    const twentyMinutesAgo = new Date(now - 20 * 60 * 1000)
    const isOffline = r.row('offline').gt(r.row('online').default(0))
    const isOld = r.row('offline').lt(twentyMinutesAgo)

    await r.table('hz_client_connections').filter(isOffline).filter(isOld).delete().run()
}
