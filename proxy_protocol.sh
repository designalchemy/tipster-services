#!/bin/bash

# Script to add loadbalancer proxy policy. Useful for accessing browser IP when
# loadbalancer is configered with TCP (i.e. for websockets)
#
# See:
# https://docs.aws.amazon.com/elasticloadbalancing/latest/classic/enable-proxy-protocol.html#proxy-protocol
# https://davidbeath.com/posts/elb-nginx-proxy-forwarding.html


# Variables.
LOAD_BALANCER_NAME='awseb-e-3-AWSEBLoa-19KGD3AMU8U3I'
NEW_POLICY_NAME='tcp-proxy-protocol'
EXISTING_POLICY_NAME=''
INSTANCE_PORT=80

# Create load balancer policy.
aws elb create-load-balancer-policy \
  --load-balancer-name $LOAD_BALANCER_NAME \
  --policy-name $NEW_POLICY_NAME \
  --policy-type-name ProxyProtocolPolicyType \
  --policy-attributes AttributeName=ProxyProtocol,AttributeValue=true

# Set load balancer policy.
aws elb set-load-balancer-policies-for-backend-server \
  --load-balancer-name $LOAD_BALANCER_NAME \
  --instance-port $INSTANCE_PORT \
  --policy-names $NEW_POLICY_NAME $EXISTING_POLICY_NAME

