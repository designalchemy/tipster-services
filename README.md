## Tipster Display, Tipster Management install -


1. Download TIPSTER-WEB and TIPSTER-SERVICES


2. Download and install RETHINKDB


3. Run `rethinkdb` (new tab)


2. Run Yarn in both of these


3. In SERVICES run the following
	- `yarn seed:dev`
	- `yarn start`


3. In WEB run the following:
	- `yarn migrate up`
	- `yarn start api` (new tab)
	- `yarn start:client` (new tab)
	- `yarn start:client-managment` (new tab)

4. Open the following urls:
	- localhost:3000/tipster/1
	- localhost:3001/tipster/management
	


## Migrations

The deployment process automagically runs database migrations for us.

To create a migration:

```
yarn migrate create my-optional-migration-name
```

This will create a file in migrations in which you can use to run commands (i.e.
database updates) the next time that the server is deployed.
