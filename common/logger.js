// Logger methods
//
//   logger.log('silly', "127.0.0.1 - there's no place like home");
//   logger.log('debug', "127.0.0.1 - there's no place like home");
//   logger.log('verbose', "127.0.0.1 - there's no place like home");
//   logger.log('info', "127.0.0.1 - there's no place like home");
//   logger.log('warn', "127.0.0.1 - there's no place like home");
//   logger.log('error', "127.0.0.1 - there's no place like home");
//   logger.info("127.0.0.1 - there's no place like home");
//   logger.warn("127.0.0.1 - there's no place like home");
//   logger.error("127.0.0.1 - there's no place like home");

const winston = require('winston')
const config = require('config')

const logger = label =>
    new winston.Logger({
        transports: [
            new winston.transports.Console({
                label: label,
                level: config.get('loggingLevel'),
                timestamp: true,
                handleExceptions: true,
                colorize: Boolean(config.colorize)
            })
        ]
    })

module.exports = logger
