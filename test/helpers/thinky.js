const config = require('config')
const _ = require('lodash')
const requireFromRoot = require('app-root-path').require
const thinky = requireFromRoot('/app/util/thinky')

// Helper functions for tests.

/**
 * Drop all the tables in the database.
 *
 * This function uses generator syntax yielding promises. This pattern is
 * used by the co library to make async javascript a little nicer to work
 * with. For more info on co, check out the repo: https://github.com/tj/co
 *
 * @return {Generator<Promise>}
 */
const dropDb = function*() {
	const tables = yield thinky.r.db(config.rethink.db).tableList()
	const promises = _.map(tables, dropTable)
	yield Promise.all(promises)
}

/**
 * Delete a table from database.
 *
 * @param {String} tableName The name of the table to be deleted.
 * @returns {Promise}
 */
const dropTable = tableName => thinky.r.table(tableName).delete()

// Setup and teardown ALL tests:

before(function*() {})

after(function*() {
	// Prevent logged output being at end of test line.
	console.log('\n')
	// Fix for  "too many files open" issue when using --watch flag.
	yield thinky.r.getPoolMaster().drain()
})

// Exposed functions
module.exports = {
	dropDb: dropDb,
}
