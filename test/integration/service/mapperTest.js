/*globals describe, beforeEach, afterEach, it */
const _ = require('lodash')
const assert = require('assert')
const requireFromRoot = require('app-root-path').require
const nock = require('nock')

const Mapper = requireFromRoot('/app/service/mapper')

let scope = null

describe('Mapper', () => {
    beforeEach(() => {
        // Setup mock endpoint...
        scope = nock('http://www.example.com')
            // Test
            .get('/test')
            .reply(200, { testResult: 'testing123' })
            // Predictor
            .get('/predictor')
            .reply(200, { predictor_id: 1 })
    })

    afterEach(() => {
        nock.cleanAll()
    })

    it('should return a promise', () => {
        const prom = new Mapper().run()
        assert(prom.then)
    })

    describe('#set', () => {
        it('can insert a constant value into results array', async () => {
            const actual = await new Mapper()
                .set('funny', 'haha')
                .set('blah.blah.blah', 'blah')
                .run()
            const expected = { funny: 'haha', blah: { blah: { blah: 'blah' } } }
            assert.deepEqual(actual, expected)
        })
    })

    describe('#fetch', () => {
        it('takes map argument to parse results', async () => {
            const actual = await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .run()

            const expected = { testResult: 'testing123' }
            assert.deepEqual(actual, expected)
        })

        it('can call fetch method multiple times to make multiple requests and return array of results', async () => {
            const actual = await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .fetch('http://www.example.com/predictor', (acc, data) =>
                    _.set(acc, 'predictor', data)
                )
                .run()

            const expected = {
                testResult: 'testing123',
                predictor: { predictor_id: 1 }
            }
            assert.deepEqual(actual, expected)
        })
    })

    describe('#catch', () => {
        it('errors if no catch present', async () => {
            try {
                await new Mapper()
                    .fetch('http://www.example.com/test', (acc, data) => data)
                    .then(() => {
                        throw new Error('This error is thrown correctly')
                    })
                    .run()
                throw new Error('Test case should not get to here.')
            } catch (e) {
                const expected =
                    'Command "then" with data {}. Message: "This error is thrown correctly"'
                assert.equal(
                    e.message,
                    expected,
                    `An error was thrown, but it was not the expected value. Instead we got "${e.message}"`
                )
            }
        })
        it('handles error if catch present', async () => {
            const actual = await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .then(() => {
                    throw new Error('This error is handled')
                })
                .catch(err => ({ error: 'Handled!' }))
                .run()
            const expected = { error: 'Handled!' }
            assert.deepEqual(actual, expected)
        })
        it('should pass first throw error to error handler', async () => {
            await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .then(() => {
                    throw new Error('This error is handled')
                })
                .then(() => {
                    throw new Error('This error should never be known')
                })
                .catch(err => {
                    // Assert that we get the correct error message
                    assert.equal(err.message, 'This error is handled')
                })
                .run()
        })
        it('should not run code that happens between a throw and a catch', async () => {
            let functionIsCalled = false
            await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .then(() => {
                    throw new Error('This error is handled')
                })
                .then(() => {
                    functionIsCalled = true
                })
                .catch(err => ({ error: 'Handled!' }))
                .run()

            assert.equal(
                functionIsCalled,
                false,
                'Code between first error and next catch should not be run'
            )
        })
        it('errors if no error thrown after a catch', async () => {
            try {
                await new Mapper()
                    .fetch('http://www.example.com/test', (acc, data) => data)
                    .catch(e => null)
                    .then(() => {
                        throw new Error('This error is thrown correctly')
                    })
                    .run()
                throw new Error('Test case should not get to here.')
            } catch (e) {
                const expected =
                    'Command "then" with data {}. Message: "This error is thrown correctly"'
                assert.equal(
                    e.message,
                    expected,
                    `An error was thrown, but it was not the expected value. Instead we got "${e.message}"`
                )
            }
        })
        it('should not run second catch if no error has occurred since the first', async () => {
            let functionIsCalled = false
            await new Mapper()
                .fetch('http://www.example.com/test', (acc, data) => data)
                .then(() => {
                    throw new Error('This error is handled')
                })
                .catch(err => ({ error: 'Handled!' }))
                .catch(err => {
                    functionIsCalled = true
                })
                .run()

            assert.equal(
                functionIsCalled,
                false,
                'Second catch function was run despite no new error occurring'
            )
        })
    })
})
