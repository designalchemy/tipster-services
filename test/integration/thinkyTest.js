const assert = require('assert')
const requireFromRoot = require('app-root-path').require
const thinky = requireFromRoot('/app/util/thinky')

describe('Thinky connection', () => {
	it('should connect to the db', function*() {
		// Will timeout if cannot connect.
		yield thinky.dbReady()
	})
})
