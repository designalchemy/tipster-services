const assert = require('assert')
const Parser = require('../../script/logParser')

describe('Parser', () => {
    describe('IpAddress', () => {
        it('should be able to parse a valid IP address', () => {
            const actual = Parser.IpAddress.parse('69.89.31.226')
            const expected = { status: true, value: '69.89.31.226' }
            assert.deepEqual(actual, expected)
        })
        it('should be not be able to parse a number over 255', () => {
            const actual = Parser.IpAddress.parse('256.89.31.226')
            const expected = {
                status: false,
                expected: ['IP Digits must be below 255'],
                index: { column: 4, line: 1, offset: 3 }
            }
            assert.deepEqual(actual, expected)
        })
        it('should not accept ip address with too few blocks', () => {
            const actual = Parser.IpAddress.parse('69.89.31')
            const expected = {
                status: false,
                expected: ["'.'"],
                index: { column: 9, line: 1, offset: 8 }
            }
            assert.deepEqual(actual, expected)
        })
        it('should not accept ip address with too many blocks', () => {
            const actual = Parser.IpAddress.parse('69.89.31.12.123')
            const expected = {
                status: false,
                expected: ['EOF'],
                index: { column: 12, line: 1, offset: 11 }
            }
            assert.deepEqual(actual, expected)
        })
    })

    describe('Request', () => {
        it('Parse a request', () => {
            const actual = Parser.Request.parse(
                '"GET http://54.171.59.237:80/mysql/admin/ HTTP/1.0"'
            )
            const expected = {
                status: true,
                value: {
                    method: 'GET',
                    protocol: 'HTTP/1.0',
                    resource: 'http://54.171.59.237:80/mysql/admin/'
                }
            }
            assert.deepEqual(actual, expected)
        })
    })

    describe('TimeStamp', () => {
        it('Parse a timestamp', () => {
            const actual = Parser.TimeStamp.parse('[27/Jul/2017:12:43:27 +0000]')
            const expected = {
                status: true,
                value: Date('27/Jul/2017:12:43:27 +0000')
            }
            assert.deepEqual(actual, expected)
        })
    })

    describe('Message', () => {
        it('should parse valid message', () => {
            const actual = Parser.Message.parse(
                '85.105.29.241 - unauthenticated [27/Jul/2017:12:43:27 +0000] "GET http://54.171.59.237:80/mysql/admin/ HTTP/1.0" 200 1255 "-" "Mozilla/5.0 Jorgee"'
            )
            const expected = {
                status: true,
                value: {
                    ipAddress: '85.105.29.241',
                    timestamp: Date('27/Jul/2017:12:43:27 +0000'),
                    statusCode: 200,
                    transferred: 1255,
                    deviceId: null,
                    request: {
                        method: 'GET',
                        resource: 'http://54.171.59.237:80/mysql/admin/',
                        protocol: 'HTTP/1.0'
                    },
                    url: '-',
                    client: 'Mozilla/5.0 Jorgee'
                }
            }
            assert.deepEqual(actual, expected)
        })
        it('valid another valid message', () => {
            const actual = Parser.Message.parse(
                '159.203.182.22 - D4B1872D-3B04-454E-8832-3D6A5FE86D61 [18/Aug/2017:14:07:04 +0000] "GET /gantry/ HTTP/1.1" 200 19116 "https://tipster.racingpost.com/gantry" "Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/98 Safari/537.4 (StatusCake)"'
            )
            const expected = {
                status: true,
                value: {
                    ipAddress: '159.203.182.22',
                    timestamp: Date('18/Aug/2017:14:07:04 +0000'),
                    statusCode: 200,
                    transferred: 19116,
                    deviceId: 'D4B1872D-3B04-454E-8832-3D6A5FE86D61',
                    request: {
                        method: 'GET',
                        resource: '/gantry/',
                        protocol: 'HTTP/1.1'
                    },
                    url: 'https://tipster.racingpost.com/gantry',
                    client: 'Mozilla/5.0 (Windows NT 6.2; WOW64) AppleWebKit/537.4 (KHTML, like Gecko) Chrome/98 Safari/537.4 (StatusCake)'
                }
            }
            assert.deepEqual(actual, expected)
        })
    })
})
